---
title: About
weight: 2
url: /about/
bookToc: true
---

<div class="wrapper">
  <div class="box sidebar">
    <img src="/images/bio.jpg">
  </div>
  <div class="box content">
b. 2000 Guadalajara, Mexico 

Sofia Arev is a figurative artist based in Toronto, Canada.  

Having studied classical art at the Academy of Realist Art in Toronto, her work draws from masters’ techniques of the 19th century to explore the figure in contemporary spaces. 

By integrating photographic elements into her work, Sofia reflects the themes that comprise the culture of her generation that is greatly shaped by nostalgia and the use of the camera.

Sofia’s paintings observe the changes of a fast-paced world that reflect in the emotions and behaviour of the youth in day to day life. Currently, her work explores the figure and its relationship with the home and personal spaces.
  </div>
</div>
