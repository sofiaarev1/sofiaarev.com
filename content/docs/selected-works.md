---
title: Selected Works
weight: 1
url: /works/
bookToc: true
---

<p class="selected-works">
    
 
</p>

<div class="selected-works works-description"> 

<p>
<img src="/images/1.jpg">
Girls Room </br>
20” x 30" </br>
oil on linen, 2023 </br>
</br>
</p>

<p>
<img src="/images/2.jpg" loading="lazy" >
Apartment Hallway </br>
36” x 36" </br>
oil on wood panel, 2023 </br>
</br></br>
</p>

<p>
<img src="/images/4.jpg" loading="lazy" >
Him and I on the Couch </br>
8” x 12” </br>
oil on aluminum, 2023 </br>
</br>
</p>

<p>
<img src="/images/5.jpg" loading="lazy" >
I Can Feel Time Moving </br>
12” x 14.5” </br>
pastel on paper mounted on matboard, 2024 </br>
</br>
</p>


</div>
